# SPDX-FileCopyrightText: 2020 Intevation GmbH
#
# SPDX-License-Identifier: BSD-3-Clause

"""example of use of the mail_producer module

Authors: Ludwig Reiter <ludwig.reiter@intevation.de>
"""
from mail_producer import create_mail, create_mail_encrypted
import os
import shutil
import tempfile
import gpg
# import smtplib


# mail without signature, uses create mail.
email = create_mail("test@example.com", "test2@example.org", "Test Mail",
                    "Testy Testy Test Body", None, None)


print(email)

# Send the message via our own SMTP server.
# s = smtplib.SMTP('localhost', 1025)
# s.send_message(email)
# s.quit()

# mail with signature


# build a temp gpg home and import keys.
_gpghome = tempfile.mkdtemp(prefix='tmp.gpghome')
os.environ['GNUPGHOME'] = _gpghome


# import requested keys into the keyring
def keyfile(key):
    keydir = os.path.join(os.path.dirname(__file__), 'keys')
    return open(os.path.join(keydir, key), 'rb')


ctx = gpg.Context()


import_keys = ['test1.sec', 'test1.pub', 'test2.gpg', 'test2.pub']
for key in import_keys:
    with keyfile(key) as fp:
        ctx.key_import(fp)

# now prepare key and ctx for signing.
key = ctx.get_key('5F503EFAC8C89323D54C252591B8CD7E15925678')
ctx.signers = [key]

try:
    # create mail with signature using create_mail
    email = create_mail("test1.intelmq@example.org",
                        "test2@example.org", "Test Mail 2",
                        "Testy Testy Test Body", None, ctx)
    print(email)
    # Send the message via our own SMTP server.
    # s = smtplib.SMTP('localhost', 1025)
    # s.send_message(email)
    # s.quit()

    # create encrypted mail
    ctx.signers = []
    recipient_key = ctx.get_key('2D22891B0EA69FBCA1808F2926F69225D182BFCE')
    email = create_mail_encrypted("test1.intelmq@example.org",
                                  "test2@example.org",
                                  recipient_key,
                                  "Test Mail 7",
                                  "Testy Testy Test Body\n", None, ctx)
    print(email)
    # Send the message via our own SMTP server.
    # s = smtplib.SMTP('localhost', 1025)
    # s.send_message(email)
    # s.quit()

# clean up GNUPGHOME and remove the temporary _gpghome
finally:
    del os.environ['GNUPGHOME']
    shutil.rmtree(_gpghome, ignore_errors=True)
